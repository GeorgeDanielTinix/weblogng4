import { WeblogPage } from './app.po';

describe('weblog App', () => {
  let page: WeblogPage;

  beforeEach(() => {
    page = new WeblogPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
