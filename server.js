const express = require('express');
const app = express();
const path = require('path')

app.use(express.static(__dirname + '/dist'));
app.listem(process.env.PORT || 8080);

// PathLocationStrategy buscando el index
app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname + '/dist/index.html'));
})

// mostrar en consola
console.log('Console listerning..!!!');