import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//  Service
import { PostsService } from "./services/posts.service";

//Router
import { WEBLOG_ROUTING } from './app.routes'

import { AppComponent } from './app.component';
import { HeadersComponent } from './components/headers/headers.component';
import { HomeComponent } from './components/home/home.component';
import { PostsComponent } from './components/posts/posts.component';

@NgModule({
  declarations: [
    AppComponent,
    HeadersComponent,
    HomeComponent,
    PostsComponent
  ],
  imports: [
    BrowserModule,
    // FormsModule,
    // HttpModule,
    WEBLOG_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
