import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { PostsComponent } from './components/posts/posts.component';

const routes: Routes = [
{ path: 'home', component: HomeComponent},
{ path: 'posts', component: PostsComponent},
{ path: '**', redirectTo: 'home' }
];

export const WEBLOG_ROUTING = RouterModule.forRoot(routes);
