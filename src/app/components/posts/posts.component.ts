import { Component, OnInit } from '@angular/core';

// Service
import { PostsService,PostsRules } from '../../services/posts.service';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styles: []
})
export class PostsComponent implements OnInit {
 posts: PostsRules[] = []; 
  constructor(private _posts: PostsService) { }

  ngOnInit() {
    this.posts = this._posts.obtenerPosts();
  }

}
