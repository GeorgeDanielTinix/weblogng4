import { Injectable } from '@angular/core';

export interface PostsRules {
  id: number;
  nombre: string;
  imagen: string;
  tipo: string;
  bio: string;
}

@Injectable()
export class PostsService {

  posts:PostsRules[] = [
    {
      'id':0,
      'nombre':'Angular 4',
      'imagen':'angular.svg',
      'tipo':'Logo',
      'bio':'Angular 4 un framework facil de usar y potente pero todavia no supera a Ruby on Rails'
    },
    {
      'id':1,
      'nombre':'Angular 4',
      'imagen':'angular.svg',
      'tipo':'Logo',
      'bio':'Angular 4 un framework facil de usar y potente pero todavia no supera a Ruby on Rails'
    },
    {
      'id':2,
      'nombre':'Angular 4',
      'imagen':'angular.svg',
      'tipo':'Logo',
      'bio':'Angular 4 un framework facil de usar y potente pero todavia no supera a Ruby on Rails'
    },
    {
      'id':3,
      'nombre':'Angular 4',
      'imagen':'angular.svg',
      'tipo':'Logo',
      'bio':'Angular 4 un framework facil de usar y potente pero todavia no supera a Ruby on Rails'
    },
    {
      'id':4,
      'nombre':'Angular 4',
      'imagen':'angular.svg',
      'tipo':'Logo',
      'bio':'Angular 4 un framework facil de usar y potente pero todavia no supera a Ruby on Rails'
    },
  ];

    obtenerPosts(){
      return this.posts;
    }
  constructor() { }

}
